import { BiomeCore, } from '../core/Biome'

import { DesertGround, } from '../entities/DesertGround'
import { OceanGround, } from '../entities/OceanGround'

export class DesertBiome extends BiomeCore {
  constructor (game, params) {
    super(game, {
      name: 'DesertBiome',
      Ground: DesertGround,
      Divider: OceanGround,
      ...params,
    })
  }
}
