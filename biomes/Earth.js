import { BiomeCore, } from '../core/Biome'

import { EarthGround, } from '../entities/EarthGround'
import { OceanGround, } from '../entities/OceanGround'

export class EarthBiome extends BiomeCore {
  constructor (game, params) {
    super(game, {
      name: 'EarthBiome',
      Ground: EarthGround,
      Divider: OceanGround,
      ...params,
    })
  }
}
