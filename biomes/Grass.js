import { BiomeCore, } from '../core/Biome'

import { GrassGround, } from '../entities/GrassGround'
import { OceanGround, } from '../entities/OceanGround'

export class GrassBiome extends BiomeCore {
  constructor (game, params) {
    super(game, {
      name: 'GrassBiome',
      Ground: GrassGround,
      Divider: OceanGround,
      ...params,
    })
  }
}
