import { BiomeCore, } from '../core/Biome'

import { RockGround, } from '../entities/RockGround'
import { OceanGround, } from '../entities/OceanGround'

export class RockBiome extends BiomeCore {
  constructor (game, params) {
    super(game, {
      name: 'RockBiome',
      Ground: RockGround,
      Divider: OceanGround,
      ...params,
    })
  }
}
