import { BiomeCore, } from '../core/Biome'

import { SnowGround, } from '../entities/SnowGround'
import { OceanGround, } from '../entities/OceanGround'

export class SnowBiome extends BiomeCore {
  constructor (game, params) {
    super(game, {
      name: 'SnowBiome',
      Ground: SnowGround,
      Divider: OceanGround,
      ...params,
    })
  }
}
