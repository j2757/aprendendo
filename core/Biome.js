import Phaser from 'phaser'
import Probability from 'probability-node'

import uuid from 'tiny-uuid4'

/**
 * @class
 * @name BiomeCore
 * @description Gerador de biomas | Biome generator
 *
 * @property {object} game Instância do Phaser | Phaser instance
 * @property {string} id Id do bioma | biome id
 *
 * @property {object[]} geology Todos os tilts do bioma | All biome tilts
 * @property {object[]} tiltBorders Tilts da borda do bioma | Biome edge tilts
 * @property {{x:[object, object], y:[object, object]}} extremeBorders Tilts nos extremos do bioma | Tilts at the extremes of the biome
 * @property {object} Ground Entidade a ser usada no terreno | Entity to be used in the field
 * @property {object} Divider Entidade a ser usada no divisor | Entity to be used in the splitter
 * @property {string} name Nome do bioma | Biome name
 * @property {number} size Tamanho do bioma | Biome size
 * @property {{x: number, y: number}} center Posição central do bioma | Biome central position
 *
 * @property {object[]} biomeMolds Formatos de biomas | Biome formats* @property {number} params Contador para identificar entre vários biomas do mesmo tipo | Counter to identify between multiple biomes of the same type
 */
export class BiomeCore {
  constructor (game, params = {}) {
    this.game = game
    this.id = uuid()

    this.geology = {}
    this.tiltBorders = []
    this.extremeBorders = { x: [ null, null, ], y: [ null, null, ], }
    this.Ground = params.Ground
    this.Divider = params.Divider
    this.count = params.count || ''
    this.name = params.name + this.count
    this.branch = params.branch || 0
    this.size = params.size || null
    this.center = { x: null, y: null, }

    this.biomeMolds = [
      {
        name: 'circle',
        top: Phaser.Math.Between(30, 70),
        right: Phaser.Math.Between(30, 70),
        bottom: Phaser.Math.Between(30, 70),
        left: Phaser.Math.Between(30, 70),
      },
      {
        name: 'h-cylinder',
        top: Phaser.Math.Between(1, 5),
        right: Phaser.Math.Between(1, 90),
        bottom: Phaser.Math.Between(1, 5),
        left: Phaser.Math.Between(1, 90),
      },
      {
        name: 'v-cylinder',
        top: Phaser.Math.Between(1, 90),
        right: Phaser.Math.Between(1, 5),
        bottom: Phaser.Math.Between(1, 90),
        left: Phaser.Math.Between(1, 5),
      },
      {
        name: 'dynamic',
        top: Phaser.Math.Between(1, 99),
        right: Phaser.Math.Between(1, 99),
        bottom: Phaser.Math.Between(1, 99),
        left: Phaser.Math.Between(1, 99),
      },
    ]
  }

  /* -------------------------------------------------------------------------------------------------------------- */

  /**
   * @method
   * @public
   * @name create
   * @description Cria o bioma
   * @param {object} config Configurações do bioma
   * @param {number} config.size Tamanho do bioma em tilts
   * @param {{x: number, y: number}} config.init Posição onde iniciar a geração do bioma | Position where to start the biome generation
   * @param {string|string[]} config.mold Formatos aceitos para esse bioma | Supported formats for this biome
   * @returns {void}
   */
  create (config) {
    this.game.debug({ msg: 'Criando ' + this.name, })

    const { size, init = { x: 0, y: 0, }, mold, limit, } = config

    this.size = size
    this.center = init

    const directions = [ 'top', 'left', 'bottom', 'right', ]
    const acceptedFormats = !mold ? '*' : typeof mold === 'object' ? mold : [ mold, ]
    const formats = (
      acceptedFormats === '*' ? this.biomeMolds : this.biomeMolds.filter(i => acceptedFormats.includes(i.name))
    )
    const format = formats[Phaser.Math.Between(0, formats.length - 1)]

    let repeat = 0

    const ground = (new (this.Ground)(this.game))
      .setPos({ x: init.x, y: init.y, })

    this.geology[init.x + 'X' + init.y] = ground
    this.extremeBorders.x[0] = ground
    this.extremeBorders.x[1] = ground
    this.extremeBorders.y[0] = ground
    this.extremeBorders.y[1] = ground

    this.tiltBorders.push(ground)

    while (this.count < size) {
      const tiltBorder = this.tiltBorders.shift()

      if (!tiltBorder) {
        Object.values(this.geology).forEach((tilt) => {
          delete this.game.objects[tilt.id]
        })

        this.game.debug({ msg: 'Sem bordas', type: 'warn', })

        return false
      }

      for (let direction = 0; direction < 4 && this.count < size; direction++) {
        const directionStr = directions[direction]
        let posX = tiltBorder.pos.x
        let posY = tiltBorder.pos.y
        let pos

        if (directionStr === 'top') {
          posY--

          pos = posY
        } else if (directionStr === 'left') {
          posX++

          pos = posX
        } else if (directionStr === 'bottom') {
          posY++

          pos = posY
        } else if (directionStr === 'right') {
          posX--

          pos = posX
        }

        if (pos < 0) {
          pos *= -1
        }

        if (this.geology[posX + 'X' + posY]) {
          continue
        }

        if (posX < limit * -1 || posX > limit || posY < limit * -1 || posY > limit) {
          repeat++

          this.game.debug({ msg: this.name + ' fora dos limites ' + repeat + ' vezes', type: 'warn', })

          if (repeat > 50) {
            this.game.debug({ msg: 'Pulando ' + this.name, type: 'warn', })

            return false
          } else {
            continue
          }
        }

        if (this.game.geology[posX + 'X' + posY]) {
          continue
        }

        const neighbors = this.game
          .getNeighborPos({ x: posX, y: posY, }, { type: '*', })
          .filter(neighbor => this.game.geology[neighbor.x + 'X' + neighbor.y])

        if (neighbors.length > 0) {
          const neighborOtherBranch = neighbors
            .map(pos => this.game.geology[pos.x + 'X' + pos.y].branch !== this.branch)
            .includes(true)

          const neighborsMain = neighbors.filter((neighbor) => {
            neighbor = this.game.geology[neighbor.x + 'X' + neighbor.y] || {}

            return neighbor.branch === 0
          })

          const neighborMain = neighborsMain.length > 0
          const probabilit = (() => {
            if (neighborMain) {
              return 0
            }

            if (neighborOtherBranch) {
              return 100
            }

            return 0
          })()

          const probabilitilized = new Probability(
            {
              p: (() => probabilit + '%')(),
              f: () => true,
            },
            {
              p: (() => 100 - probabilit + '%')(),
              f: () => false,
            }
          )

          if (probabilitilized()) {
            const ground =
              (new (this.Divider)(this.game))
                .setPos({ x: posX, y: posY, })

            this.geology[posX + 'X' + posY] = ground

            continue
          }
        }

        const probabilit = format[directionStr]

        const probabilitilized = new Probability(
          {
            p: (() => probabilit + '%')(),
            f: () => true,
          },
          {
            p: (() => 100 - probabilit + '%')(),
            f: () => false,
          }
        )

        if (!probabilitilized()) {
          continue
        }

        this.count++

        const ground =
          (new (this.Ground)(this.game))
            .setPos({ x: posX, y: posY, })
            .setBranch(this.branch)

        this.geology[posX + 'X' + posY] = ground

        if (posX < this.extremeBorders.x[0].pos.x) {
          this.extremeBorders.x[0] = ground
        } else if (posX > this.extremeBorders.x[1].pos.x) {
          this.extremeBorders.x[1] = ground
        }

        if (posY < this.extremeBorders.y[0].pos.y) {
          this.extremeBorders.y[0] = ground
        } else if (posY > this.extremeBorders.y[1].pos.y) {
          this.extremeBorders.y[1] = ground
        }

        if (
          this.game.getFreeTiltSides(
            ground.pos, { ...this.game.geology, ...this.geology, }, { type: '+', }
          ).length > 0
        ) {
          this.tiltBorders.push(ground)
        }
      }

      if (
        this.game.getFreeTiltSides(
          tiltBorder.pos, { ...this.game.geology, ...this.geology, }, { type: '+', }
        ).length > 0
      ) {
        this.tiltBorders.push(tiltBorder)
      }
    }

    this.tiltBorders = this.tiltBorders.filter((tilt) => {
      return this.game.getFreeTiltSides(tilt.pos, this.geology, { type: '+', }).length > 0
    })

    return true
  }

  /* -------------------------------------------------------------------------------------------------------------- */

  /**
   * @method
   * @public
   * @name getRandomBorder
   * @description Retorna aleatóriamente uma borda do bioma que tenha ao menos um lado livre | Randomly returns a biome edge that has at least one free side
   * @param {number} attempts Número de tentativas de encontrar | Number of attempts to find
   * @returns {object}
   */
  getRandomBorder (attempts = 9) {
    const border = this.tiltBorders[Phaser.Math.Between(0, this.tiltBorders.length - 1)]

    attempts--

    return attempts === 0 || (border && this.game.getFreeTiltSides(border.pos, this.geology, { type: '+', }).length > 0)
      ? border
      : this.getRandomBorder(attempts)
  }

  /**
   * @method
   * @public
   * @name getExtremBorder
   * @description Retorna a borda de um dos 4 extremos do bioma | Returns the edge of one of the 4 ends of the biome
   * @param {string} direction Lado desejado do bioma | Desired side of the biome
   * @returns
   */
  getExtremBorder (direction) {
    if (direction === 'left') {
      return this.extremeBorders.x[0]
    } else if (direction === 'right') {
      return this.extremeBorders.x[1]
    } else if (direction === 'top') {
      return this.extremeBorders.y[0]
    } else if (direction === 'bottom') {
      return this.extremeBorders.y[1]
    }
  }

  /* -------------------------------------------------------------------------------------------------------------- */

  /**
   * @method
   * @public
   * @name sanatizeBorders
   * @description Remove da lista de bordas aquelas que são vizinhas de outro bioma | Removes from the list of borders those that are neighbors of another biome
   * @returns {void}
   */
  sanatizeBorders () {
    this.tiltBorders = this.tiltBorders.filter((tilt) => {
      return this.game.getFreeTiltSides(tilt.pos, { ...this.game.geology, ...this.geology, }, { type: '+', }).length > 0
    })
  }
}
