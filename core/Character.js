import { EntitieCore, } from './Entity'

/**
 * @class
 * @name CharacterCore
 * @description Classe responsável pelos personagens | Class responsible for the characters
 * @property {number} velocity - Velocidade do jogador | Player speed
 */
export class CharacterCore extends EntitieCore {
  constructor (game, params) {
    super(game, params)

    this.velocity = 300
    this.visible = true
    this.active = true
  }

  /* -------------------------------------------------------------------------------------------------------------- */

  /**
   * @method
   * @public
   * @name create
   * @description Cria o personagem | create the character
   * @returns {void}
   */
  create () {
    this.instance = this.game.physics.add.sprite(this.posX, this.posY, 'Character')

    this.camera = this.game.cameras.main

    this.animations()
    this.camera.startFollow(this.instance)

    this.addToGroups([
      'Character',
      'Collider',
    ])

    return super.create()
  }

  /**
   * @method
   * @public
   * @name update
   * @description Atualiza o personagem | Update the character
   * @returns {void}
   */
  update () {
    this.controls()

    return this
  }

  /* -------------------------------------------------------------------------------------------------------------- */

  /**
   * @method
   * @public
   * @name animations
   * @description Define as animações do personagem | Sets character animations
   * @returns {void}
   */
  animations () {
    this.game.anims.create({
      key: 'left',
      frames: this.game.anims.generateFrameNumbers('Character', { start: 3, end: 5, }),
      frameRate: 7,
      repeat: -1,
    })

    this.game.anims.create({
      key: 'down',
      frames: this.game.anims.generateFrameNumbers('Character', { start: 0, end: 2, }),
      frameRate: 7,
      repeat: -1,
    })

    this.game.anims.create({
      key: 'right',
      frames: this.game.anims.generateFrameNumbers('Character', { start: 6, end: 8, }),
      frameRate: 7,
      repeat: -1,
    })

    this.game.anims.create({
      key: 'up',
      frames: this.game.anims.generateFrameNumbers('Character', { start: 9, end: 11, }),
      frameRate: 7,
      repeat: -1,
    })

    this.instance.anims.play('down')
  }

  /**
   * @method
   * @public
   * @name controle
   * @description Define os controles do personagem | Sets character controls
   * @returns {void}
   */
  controls () {
    this.instance.setVelocityX(0)
    this.instance.setVelocityY(0)

    this.game.controllers.add('keyboard', 'W', () => {
      this.instance.setVelocityY(this.velocity * -1)

      this.instance.anims.play('up', true)
    })

    this.game.controllers.add('keyboard', 'S', () => {
      this.instance.setVelocityY(this.velocity)

      this.instance.anims.play('down', true)
    })

    this.game.controllers.add('keyboard', 'A', () => {
      this.instance.setVelocityX(this.velocity * -1)

      this.instance.anims.play('left', true)
    })

    this.game.controllers.add('keyboard', 'D', () => {
      this.instance.setVelocityX(this.velocity)

      this.instance.anims.play('right', true)
    })
  }

  /* -------------------------------------------------------------------------------------------------------------- */

  /**
   * @method
   * @public
   * @name setVelocity
   * @description Define a velocidade do personagem | Sets the character's speed
   * @param {number} velocity A velocidade | The velocity
   * @returns {this}
   */
  setVelocity (velocity) {
    this.velocity = velocity

    return this
  }
}
