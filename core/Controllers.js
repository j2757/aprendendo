import Phaser from 'phaser'

/**
 * @class
 * @name ControllersCore
 * @description Controles de usuário do jogo | Game user controls
 * @property {object} game Instância do Phaser | Phaser instance
 * @property {object} inactives Memória dos controles inativos | Inactive controls memory
 * @returns {void}
 */
export class ControllersCore {
  constructor (game) {
    this.game = game

    this.inactives = {}

    window.addEventListener('blur', () => this.restoreInactives())
    // window.addEventListener('focus', () => console.log('focus'))
  }

  /**
   * @method
   * @public
   * @name add
   * @description Adiciona um controle | add a control
   * @param {string} device Nome do dispositivo | Device name
   * @param {string} keyName Nome da tecla | Key name
   * @param {function} handler Função a ser executada | Function to be performed
   * @param {object} params Parâmetros extras | Extra parameters
   *  @param {number} params.delay Tempo de delay entre apertos | Delay time between squeezes
   *  @param {string} params.event Nome do evento | Event name
   *  @param {boolean} params.enable Se inicia ativo | Starts active
   * @returns {void}
   */
  add (device, keyName, handler, params = {}) {
    const { delay, event = 'checkDown', enable = true, } = params

    if (device === 'keyboard') {
      const key = this.game.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes[keyName])

      if (this.game.input.keyboard[event](key, delay)) {
        handler()
      }
    }

    if (!enable) {
      this.power(device, keyName, false)
    }
  }

  /**
   * @method
   * @public
   * @name power
   * @description Liga/Desliga o controle | Control on/off
   * @param {string} device Nome do dispositivo | Device name
   * @param {string} keyName Nome da tecla | Key name
   * @param {boolean} Se deve ligar | Whether to call
   * @returns {void}
   */
  power (device, keyName, val) {
    if (device === 'keyboard') {
      const key = this.game.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes[keyName])

      this.game.input.keyboard.keys[key.keyCode].enabled = val

      if (val) {
        this.game.input.keyboard.addCapture(keyName)

        delete this.inactives[device + '.' + keyName]
      } else {
        this.game.input.keyboard.removeCapture(keyName)

        this.inactives[device + '.' + keyName] = [ device, keyName, ]
      }
    }
  }

  /**
   * @method
   * @public
   * @name power
   * @description Liga/Desliga todos os controles, sendo possível adicionar excessões | Control on/off
   * @param {string} device Nome do dispositivo | Device name
   * @param {boolean} Se deve ligar | Whether to call
   * @param {string | string[]} exception Nome(s) das exceções | Exception name(s)
   * @returns {void}
   */
  powerAll (device, val, exception = []) {
    exception = typeof exception === 'string' ? [ exception, ] : exception

    const exceptionKeyCodes = exception.map(i => Phaser.Input.Keyboard.KeyCodes[i])

    if (device === 'keyboard') {
      this.game.input.keyboard.keys.forEach((key, keyCode) => {
        if (!exceptionKeyCodes.includes(keyCode)) {
          const keyName = Object.entries(Phaser.Input.Keyboard.KeyCodes)
            .map(([ name, code, ]) => {
              return code === keyCode ? name : undefined
            })
            .filter(i => i)
            .shift()

          this.power(device, keyName, val)
        }
      })
    }
  }

  restoreInactives () {
    this.powerAll('keyboard', false)
  }
}
