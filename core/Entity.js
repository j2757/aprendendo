import uuid from 'tiny-uuid4'

/**
 * @class
 * @name EntitieCore
 * @description Classe responsável pelos personagens | Class responsible for the characters
 *
 * @property {object} game Instância do Phaser | Phaser instance
 *
 * @property {string} id Id do objeto | object id
 * @property {boolean} created Se objeto já foi criado | If object has already been updated
 * @property {boolean} updated Se objeto já foi atualizado | If object has already been updated
 *
 * @property {object} entitie Objeto da entidade | Entity object
 * @property {string} debugMessage A mensagem de debug | The debug message
 * @property {string[]} groups  Os grupos | The groups
 *
 * @property {{x: number, y: number}} pos Posição X, Y em Tilts do objeto | X,Y Position in Object Tilts
 * @property {number} origin A origem do objeto | the origin of the object
 * @property {boolean} active Se objeto está ativo | If object is active
 * @property {boolean} visible Se objeto está visível | If object is visible
 * @property {number} depth Profundidade do objeto na tela | Depth of the object on the screen
 * @property {boolean} fixedToCamera Se objeto esta fixo em relação à tela | If object is fixed in relation to the screen
 * @property {boolean} pushable Se o objeto pode ser empurrado | If the object can be pushed
 *
 * @property {oject} params Parâmetros extras | Extra parameters
 *   @property {string} params.id Id do objeto | Object id
 *   @property {boolean} params.interface Se é um objeto de interface | If it's an interface object
 */
export class EntitieCore {
  constructor (game, params = {}) {
    this.game = game

    this.id = params.id || uuid()
    this.interface = params.interface || false
    this.created = false
    this.updated = false

    this.instance = null
    this.cameras = []
    this.debugMessage = null
    this.groups = []

    this.pos = null
    this.origin = null
    this.active = false
    this.visible = false
    this.depth = 0
    this.pushable = false
  }

  /* -------------------------------------------------------------------------------------------------------------- */

  /**
   * @method
   * @public
   * @name create
   * @description Cria o objeto da entidade | Create the entity object
   * @returns {void}
   */
  create () {
    this.setPos(this.pos)

    if (typeof this.origin === 'number') {
      this.instance.setOrigin(this.origin)
    }

    this.groups.forEach((group) => {
      this.game.groups[group].add(this)
    })

    this.setDepth(this.depth)
    this.setActive(this.active)
    this.setVisible(this.visible)
    this.setFixedToCamera(this.fixedToCamera)
    this.setPushable(this.pushable)

    if (!this.active && !this.interface) {
      this.game.removeObject(this)
    }

    this.created = true

    return this
  }

  /**
   * @method
   * @public
   * @name update
   * @description Atualiza o objeto da entidade | Update the entity object
   * @returns {void}
   */
  update () {
    this.updated = true

    if (this.controls) {
      this.controls()
    }

    return this
  }

  /* -------------------------------------------------------------------------------------------------------------- */

  /**
   * @method
   * @public
   * @name setPos
   * @description Seta a posição do objeto | Set the object's position
   * @param {{object}} pos Posição do objeto | Object position
   *  @param {{number}} pos.x Posição horizontal | Horizontal position
   *  @param {{number}} pos.y Posição vertical | Vertical position
   *  @param {{boolean}} pos.onScreen Se a posição será em relação à tela | Whether the position will be relative to the screen
   * @returns {this}
   */
  setPos (pos) {
    if (this.instance) {
      const { x, y, } = !pos.onScreen ? this.game.tiltToPos(pos) : pos

      this.instance.x = x
      this.instance.y = y
    }

    this.pos = pos

    return this
  }

  /**
   * @method
   * @public
   * @name setOrigin
   * @description Seta a origem do objeto | Set the object's origin
   * @param {{number}} origin A origem | The origin
   * @returns {this}
   */
  setOrigin (origin) {
    if (origin === 'top-left') {
      this.origin = 0
    }

    return this
  }

  /**
   * @method
   * @public
   * @name addToGroup
   * @description Adiciona objeto ao grupo | Add object to group
   * @returns {this}
   */
  addToGroup (group) {
    this.addToGroups([ group, ])

    return this
  }

  /**
   * @method
   * @public
   * @name addToGroups
   * @description Adiciona objeto aos grupos | Add object to groups
   * @returns {this}
   */
  addToGroups (groups) {
    this.groups = this.groups.concat(groups)

    return this
  }

  /**
   * @method
   * @public
   * @name destroy
   * @description Destrói o objeto | Destroy the object
   * @returns {this}
   */
  destroy () {
    if (this.instance) {
      this.instance.destroy()
    }

    this.game.removeObject(this)
  }

  /**
   * @method
   * @public
   * @name setDepth
   * @description Seta a profundidade do objeto na tela | Sets the depth of the object on the screen
   * @param {number} depth A profundidade | The depth
   * @returns {this}
   */
  setDepth (depth) {
    if (this.instance) {
      this.instance.setDepth(depth)
    }

    this.depth = depth

    return this
  }

  /**
   * @method
   * @public
   * @name setActive
   * @description Ativa/Desativa o objeto | Activate/Deactivate the object
   * @param {boolean} active Se deve ativar ou desativar o objeto | Whether to enable or disable the object
   * @returns {this}
   */
  setActive (active) {
    if (this.instance) {
      this.instance.setActive(active)
    }

    if (this.interface || active) {
      this.game.addObject(this)
    } else if (!this.interface && !active) {
      this.game.removeObject(this)
    }

    this.active = active

    return this
  }

  /**
   * @method
   * @public
   * @name on
   * @description Ativa o objeto | Active the object
   * @returns {this}
   */
  on () {
    this.setActive(true)

    return this
  }

  /**
   * @method
   * @public
   * @name off
   * @description Inativa o objeto | Deactivate the object
   * @returns {this}
   */
  off () {
    this.setActive(false)

    return this
  }

  /**
   * @method
   * @public
   * @name setVisible
   * @description Exibe/Oculta o objeto | Show/Hide the object
   * @param {boolean} visible Se deve exibir ou ocultar o objeto | Whether to show or hide the object
   * @returns {this}
   */
  setVisible (visible) {
    if (this.instance) {
      this.instance.setVisible(visible)
    }

    this.visible = visible

    return this
  }

  /**
   * @method
   * @public
   * @name show
   * @description Exibe o objeto | Show the object
   * @returns {this}
   */
  show () {
    this.setVisible(true)

    return this
  }

  /**
   * @method
   * @public
   * @name hide
   * @description Oculta o objeto | Hide the object
   * @returns {this}
   */
  hide () {
    this.setVisible(false)

    return this
  }

  /**
   * @method
   * @public
   * @name render
   * @description Renderiza o objeto (atalho para show e active) | Render the object (shortcut to show and active)
   * @returns {this}
   */
  render () {
    this.show()
    this.on()

    return this
  }

  /**
   * @method
   * @public
   * @name unrender
   * @description Desrenderiza o objeto (atalho para hide e inactive) | Unender the object (shortcut to hide and inactive)
   * @returns {this}
   */
  unrender () {
    this.hide()
    this.off()

    return this
  }

  /**
   * @method
   * @public
   * @name setFixedToCamera
   * @description Seta se o objeto estará fixo à camera | Arrow if the object will be fixed to the camera
   * @param {boolean} fixedToCamera Se o objeto estará fixo à camera | Whether the object will be fixed to the camera
   * @returns {this}
   */
  setFixedToCamera (fixedToCamera) {
    if (this.instance) {
      this.instance.setScrollFactor(fixedToCamera ? 0 : 1)
    }

    this.fixedToCamera = fixedToCamera

    return this
  }

  /**
   * @method
   * @public
   * @name fixToCamera
   * @description Seta se o objeto como fixo à camera | Arrow if object how to pin to camera
   * @returns {this}
   */
  fixToCamera () {
    this.setFixedToCamera(true)

    return this
  }

  /**
   * @method
   * @public
   * @name setPushable
   * @description Seta se o objeto pode ser empurrado | Arrow if object can be pushed
   * @returns {this}
   */
  setPushable (pushable = true) {
    if (this.instance && this.instance.setPushable) {
      this.instance.setPushable(pushable)
    }

    this.pushable = pushable

    return this
  }

  /**
   * @method
   * @public
   * @name unfixToCamera
   * @description Seta se o objeto como não fixo à camera | Arrow if the object as not fixed to the camera
   * @returns {this}
   */
  unfixToCamera () {
    this.setFixedToCamera(false)

    return this
  }

  /* -------------------------------------------------------------------------------------------------------------- */

  /**
   * @method
   * @public
   * @name setDebugMessage
   * @description Define uma mensagem de debug
   * @param {string} msg - A mensagem
   * @param {object} style - Estilo da mensagem
   * @returns {this}
   */
  setDebugMessage (msg, style = {}) {
    const { x, y, } = this.game.tiltToPos(this.pos)

    this.debugMessage = this.game.add.text(x, y, msg, {
      fontSize: '18px',
      backgroundColor: '#FF0000',
      ...style,
    }).setDepth(999)

    return this
  }
}
