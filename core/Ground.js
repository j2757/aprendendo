import { EntitieCore, } from './Entity'

/**
 * @class
 * @name GroundCore
 * @description Terreno padrão
 *
 * @property {number} depth Profundidade do objeto na tela | Depth of the object on the screen
 */
export class GroundCore extends EntitieCore {
  constructor (game) {
    super(game)

    this.depth = -1
  }

  /* -------------------------------------------------------------------------------------------------------------- */

  /**
   * @method
   * @public
   * @name create
   * @description Cria o objeto d entidade | Create the entity object
   * @returns {void}
   */
  create () {
    this.addToGroup('Ground')

    return super.create()
  }

  /**
   * @method
   * @public
   * @name update
   * @description Atualiza o objeto da entidade | Update the entity object
   * @returns {void}
   */
  update () {
    return super.update()
  }

  /* -------------------------------------------------------------------------------------------------------------- */

  /**
   * @method
   * @public
   * @name setBranch
   * @description Seta o braço na geração do mapa | Arrow arm in map generation
   * @params {number} branchNumber Número do branch | Branch number
   * @returns {this}
   */
  setBranch (branchNumber) {
    this.branch = branchNumber

    return this
  }
}
