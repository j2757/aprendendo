import Phaser from 'phaser'

/**
 * @class
 * @name GroupCore
 * @description Agrupa objetos | Groups objects
 * @property {object} object Phaser Grupos | Phaser Group
 * @property {object[]} objects Os objetos | The objects
 */
export class GroupCore {
  constructor (game) {
    this.game = game
    this.instance = new Phaser.GameObjects.Group()

    this.objects = []
  }

  add (object) {
    this.objects[object.id] = object

    this.instance.add(object.instance)
  }

  list () {
    return Object.values(this.objects)
  }

  ids () {
    return Object.keys(this.objects)
  }
}
