import Phaser from 'phaser'

import { CommandLine, CommandLineEntity, } from '../entities/CommandLine.js'
import { ControllersCore, } from './Controllers'
import { GroupCore, } from './Group'

/**
 * @class
 * @name WorldCore
 * @description Classe responsável pela geração de mundos | Class responsible for generating worlds
 *
 * @property {boolean} activeDebug - Se deve exibir mensagens de debug | Whether to display debug messages
 *
 * @property {object[]} entities Entidades do mundo | Entities of the world
 * @property {object[]} objects Objetos do mundo | Objects of the world
 * @property {object[]} groups Os grupos | The groups
 *
 * @property {object[]} geology Todos os tilts do mundo | All the tilts in the world
 * @property {object} MainBiome Biomas a ser usado como principal | Biomes to be used as main
 * @property {object[]} biomes Lista de biomas a serem usados | List of biomes to use
 * @property {object} createdBiomes Lista de biomas criados | List of created biomes
 *
 * @property {number} mapSize Tamanho do mundo em tilts | World size in tilts
 * @property {number} tiltSize Tamanho do tilt em pixels | Tilt size in pixels
 * @property {number} zoom Zoom da câmera | Camera zoom
 * @property {number} zoomMin Zoom mínimo da câmera | Minimum camera zoom
 * @property {number} zoomMax Zoom áximo da câmera | Maximum camera zoom
 * @property {number} zoomLimitEnabled Se o limite de zoom está habilitado | If the zoom limit is enabled
 *
 * @property {{x: number, y: number}} charPos - Posição do jogador | Player position
 */
export class WorldCore extends Phaser.Scene {
  constructor (config) {
    super(config)

    this.activeDebug = false

    this.entities = []
    this.objects = {}
    this.groups = {}

    this.geology = {}
    this.MainBiome = null
    this.biomes = []
    this.createdBiomes = {}
    this.DividerGround = null

    this.mapSize = 20
    this.tiltSize = 180
    this.zoom = 0.8
    this.zoomMin = 0.5
    this.zoomMax = 1.75
    this.rangeRendering = 30
    this.zoomLimitEnabled = true

    this.charPos = { x: null, y: null, }
  }

  /* -------------------------------------------------------------------------------------------------------------- */

  /**
   * @method
   * @public
   * @name preload
   * @description Carrega mídias | Load media
   * @returns {void}
   */
  preload () {
    this.controllers = new ControllersCore(this)

    this.cameras.add(0, 0, 0, 0, false, 'interface')

    this.cameras.main.setZoom(this.zoom)
    this.cameras.getCamera('interface').setZoom(this.zoom)

    this.addEntities([
      CommandLineEntity,
    ])

    this.createGroups([
      'Interface',
      'Collider',
      'Ground',
      'DividerGround',
      'Character',
      'Border',
    ])

    this.entities.forEach((loadEntity) => {
      loadEntity(this)
    })
  }

  /**
   * @method
   * @public
   * @name create
   * @description Cria objetos | Create objects
   * @returns {void}
   */
  create () {
    this.addObjects([
      (new CommandLine(this, { id: 'CommandLine', interface: true, }))
        .setPos({ x: 15, y: 15, onScreen: true, })
        .setOrigin('top-left'),
    ])

    this.generateMap()

    Object.values(this.objects).forEach((object) => {
      object.create()
    })

    this.groups.Border.list().forEach((i) => {
      i.instance.alpha = 0.5
    })

    this.physics.add.collider(this.groups.Collider.instance, this.groups.Character.instance)
  }

  /**
   * @method
   * @public
   * @name update
   * @description Atualiza objetos | Update objects
   * @returns {void}
   */
  update () {
    Object.values(this.objects).forEach((object) => {
      this.renderingArea()

      object.update()
    })
  }

  /* -------------------------------------------------------------------------------------------------------------- */

  /**
   * @method
   * @public
   * @name controls
   * @description Define os controles do mundo | Sets world controls
   * @returns {void}
   */
  controls () {
    this.controllers.add('keyboard', 'NUMPAD_ADD', () => this.zoomIn(), { delay: 50, })
    this.controllers.add('keyboard', 'NUMPAD_SUBTRACT', () => this.zoomOut(), { delay: 50, })
  }

  /* -------------------------------------------------------------------------------------------------------------- */

  /**
   * @method
   * @public
   * @name addEntity
   * @description Adiciona uma entidade ao mundo | Adds an entity to the world
   * @param {description} entity - Carregador da entidade | Entity Loader
   * @returns {void}
   */
  addEntity (entity) {
    this.addEntities([ entity, ])
  }

  /**
   * @method
   * @public
   * @name addEntities
   * @description Adiciona entidades ao mundo | Add entities to the world
   * @param {description[]} entities - Carregadores das entidades | Entity Loaders
   * @returns {void}
   */
  addEntities (entities) {
    this.entities = [ ...this.entities, ...entities, ]
  }

  /**
   * @method
   * @public
   * @name addObject
   * @description Adiciona um objeto ao mundo | Adds an object to the world
   * @param {object} object - O objeto | The object
   * @returns {void}
   */
  addObject (object) {
    this.addObjects([ object, ])

    return object
  }

  /**
   * @method
   * @public
   * @name addObjects
   * @description Adiciona objetos ao mundo | Add objects to the world
   * @param {object[]} objects - Os objetos | The objects
   * @returns {void}
   */
  addObjects (objects) {
    objects.forEach((object) => {
      this.objects[object.id] = object
    })

    return objects
  }

  /**
   * @method
   * @public
   * @name removeObject
   * @description Remove um objeto | Remove an object
   * @param {object} object - O objeto | The object
   * @returns {void}
   */
  removeObject (object) {
    this.removeObjects([ object, ])
  }

  /**
   * @method
   * @public
   * @name removeObjects
   * @description Remove objetos | Remove objects
   * @param {object[]} objects - Os objeto | The objects
   * @returns {void}
   */
  removeObjects (objects) {
    objects.forEach((object) => {
      delete this.objects[object.id]
    })
  }

  /**
   * @method
   * @public
   * @name addMainBiome
   * @description Define um bioma principal para o mundo | Sets a main biome for the world
   * @param {object} biome - O bioma | The biome
   * @returns {void}
   */
  addMainBiome (biome) {
    this.MainBiome = biome
  }

  /**
   * @method
   * @public
   * @name addBiome
   * @description Define um bioma para o mundo | Sets a biome for the world
   * @param {object} biome - O bioma | The biome
   * @returns {void}
   */
  addBiome (biome) {
    this.addBiomes([ biome, ])
  }

  /**
   * @method
   * @public
   * @name addBiomes
   * @description Define biomas para o mundo | Sets biomes for the world
   * @param {object[]} biomes - Os bioma | The biomes
   * @returns {void}
   */
  addBiomes (biomes) {
    biomes.forEach((biome) => {
      if (!biome.Biome) {
        biome = { Biome: biome, }
      }

      biome = { count: 1, success: null, separable: true, ...biome, }

      for (; biome.count > 0; biome.count--) {
        this.biomes.push({ ...biome, })
      }
    })
  }

  /**
   * @method
   * @public
   * @name createGroup
   * @description Cria um grupo | Create a group
   * @param {string} group - Nome do grupo | Group's name
   * @returns {void}
   */
  createGroup (group) {
    this.createGroups([ group, ])
  }

  /**
   * @method
   * @public
   * @name createGroups
   * @description Cria grupos | Create groups
   * @param {string} groups - Group names | Nomes dos grupos
   * @returns {void}
   */
  createGroups (groups) {
    groups.forEach((group) => {
      this.groups[group] = new GroupCore(this)
    })
  }

  /* -------------------------------------------------------------------------------------------------------------- */

  /**
   * @method
   * @public
   * @name generateMap
   * @description Gera o mapa do mundo | Generate the world map
   * @returns {void}
   */
  generateMap () {
    const generateBranch = (PreviousBiome, branchNumber) => {
      const sortPos = () => {
        const randomBorder = PreviousBiome.getRandomBorder()

        if (!randomBorder) {
          return false
        }

        const randomBorderFreeSides = this.getFreeTiltSides(randomBorder.pos, this.geology, { type: '+', })

        if (randomBorderFreeSides.length === 0) {
          return sortPos()
        }

        const randomPos = randomBorderFreeSides[Phaser.Math.Between(0, randomBorderFreeSides.length - 1)]

        return randomPos
      }

      const randomPos = sortPos()
      let BranchBiome = { success: false, }

      if (randomPos) {
        BranchBiome = this.biomes.shift()

        const BranchBiomeMap = new (BranchBiome.Biome)(this, {
          count: BranchBiome.count,
          branch: branchNumber,
        })
        const size = continentSize * Phaser.Math.FloatBetween(0.003, 0.03)

        const success = BranchBiomeMap.create({
          size: BranchBiome.size || size,
          init: randomPos,
          limit: this.limit() * continentProportion,
          mold: [ 'dynamic', ],
        })

        if (success || (BranchBiome.separable && BranchBiomeMap.size >= 50)) {
          createdBiomes.forEach((biome) => {
            biome.sanatizeBorders()

            this.geology = { ...this.geology, ...biome.geology, }
          })

          branchs.push(BranchBiomeMap)
          createdBiomes.push(BranchBiomeMap)

          BranchBiome.success = true

          this.geology = { ...this.geology, ...BranchBiomeMap.geology, }

          if (!success && BranchBiome.separable && BranchBiomeMap.size >= 50) {
            this.debug({ msg: 'Partindo ' + BranchBiomeMap.name, type: 'warn', })

            BranchBiome.size = size - BranchBiomeMap.size
            BranchBiome.count = `${BranchBiome.count}`.split('-')
            BranchBiome.count[1] = (Number(BranchBiome.count[1]) || 0) + 1
            BranchBiome.count = BranchBiome.count.join('-')

            this.biomes.push(BranchBiome)
          }

          this.debug({ msg: 'Biomas Criados:' + createdBiomes.length, style: 'color: green', })
        } else {
          BranchBiome.success = null

          this.biomes.push(BranchBiome)
        }
      }

      return BranchBiome
    }

    const BiomeMap = new (this.MainBiome)(this, { count: 0, })
    const branchs = []
    const createdBiomes = []
    const continentProportion = 0.60
    const continentSize = this.totalTilts() * continentProportion

    BiomeMap.create({
      size: continentSize * Phaser.Math.FloatBetween(0.013, 0.023),
      limit: this.limit() * continentProportion,
      mold: [ 'dynamic', ],
    })

    this.geology = { ...this.geology, ...BiomeMap.geology, }

    createdBiomes.push(BiomeMap)

    this.biomes = this.biomes.sort(() => Phaser.Math.Between(-1, 1))

    for (let i = 0, max = 4; i < max; i++) {
      const Branch = generateBranch(BiomeMap, i + 1)

      if (Branch.success === false) {
        i--
      }
    }

    while (this.biomes.length > 0) {
      const PreviousBiome = branchs.shift()
      const Branch = generateBranch(PreviousBiome, PreviousBiome.branch)

      if (!Branch.success) {
        branchs.push(PreviousBiome)
      }
    }

    for (let x = this.limit() * -1, maxX = this.limit(); x < maxX; x++) {
      for (let y = this.limit() * -1, maxY = this.limit(); y < maxY; y++) {
        this.debug({ msg: x + 'X' + y, })
        if (!this.geology[x + 'X' + y]) {
          this.geology[x + 'X' + y] = (new (this.DividerGround)(this))
            .setPos({ x, y, })
        }
      }
    }

    this.addObjects(Object.values(this.geology))

    createdBiomes.forEach((biome) => {
      biome.tiltBorders.forEach((tiltBorder) => {
        tiltBorder.addToGroups([ 'Border', ])
        this.getNeighborPos(tiltBorder.pos, { type: '+', }).forEach(({ x, y, }) => {
          const ground = this.geology[x + 'X' + y]

          if (ground.constructor.name === this.DividerGround.name) {
            ground.addToGroups([ 'Collider', 'DividerGround', ])
          }
        })
      })

      this.createdBiomes[biome.name] = {
        id: biome.id,
        name: biome.name,
        center: biome.center,
        tiltCount: Object.keys(biome.geology).length,
      }
    })
  }

  /**
   * @method
   * @public
   * @name renderingArea
   * @description Ativa/Inatival objetos que estejam dentro/fora da área de renderização | Enables/Disables objects that are inside/outside the render area
   * @property {boolean} force Força a executar mesmo que não tenha mudado de posição | Forces to run even if not changed position
   * @returns {void}
   */
  renderingArea (force) {
    const char = this.groups.Character.list()[0].instance
    const charPos = this.posToTilt({ x: char.x, y: char.y, })
    const range = this.rangeRendering

    if (!force && charPos.x === this.charPos.x && charPos.y === this.charPos.y) {
      return
    }

    this.charPos = charPos

    for (let x = charPos.x - range - 1, minX = x, maxX = charPos.x + range + 1; x <= maxX; x++) {
      for (let y = charPos.y - range - 1, minY = y, maxY = charPos.y + range + 1; y <= maxY; y++) {
        const ground = this.geology[x + 'X' + y]

        if (!ground) {
          continue
        }

        if (x === minX || x === maxX || y === minY || y === maxY) {
          ground.unrender()
        } else {
          ground.render()
        }
      }
    }
  }

  /* -------------------------------------------------------------------------------------------------------------- */

  /**
   * @method
   * @public
   * @name tiltToPos
   * @description Converte uma posição de tilt para pixel | Convert a tilt position to pixel
   * @param {{x: number, y: number}} Posição em tilt | Tilt position
   * @returns {{x, y}} Posição em pixels | Position in pixels
   */
  tiltToPos (pos) {
    const { x, y, } = pos

    return {
      x: (x * this.tiltSize) + this.tiltSize / 2,
      y: (y * this.tiltSize) + this.tiltSize / 2,
    }
  }

  /**
   * @method
   * @public
   * @name posToTilt
   * @description Converte uma posição de pixel para tilt | Convert a pixel position to tilt
   * @param {{x: number, y: number}} Posição em pixel | Pixel position
   * @returns {{x, y}} Posição em tilts | Position in tilts
   */
  posToTilt (pos) {
    const { x, y, } = pos

    return {
      x: parseInt((x / (this.tiltSize / 1))),
      y: parseInt((y / (this.tiltSize / 1))),
    }
  }

  /**
   * @method
   * @name getNeighborPos
   * @description Retorna as posições laterais a informada | Returns the side positions given
   * @param {{x: number, y: number}} pos Posição a conferir (em tilts) | Position to check (on tilts)
   * @param {object} params Parâmetros extras | Extra parameters
   *   @param {'+' | '-' | '*'} params.type Direções a serem conferidas | Directions to be given
   * @returns {{x: number, y: number}[]}
   */
  getNeighborPos ({ x, y, }, params) {
    const neighbors = []
    const { type, } = params

    if ([ '+', '*', ].includes(type)) {
      neighbors.push({ x, y: y - 1, })
      neighbors.push({ x: x + 1, y, })
      neighbors.push({ x, y: y + 1, })
      neighbors.push({ x: x - 1, y, })
    }

    if ([ 'x', 'X', '*', ].includes(type)) {
      neighbors.push({ x: x + 1, y: y - 1, })
      neighbors.push({ x: x + 1, y: y + 1, })
      neighbors.push({ x: x - 1, y: y + 1, })
      neighbors.push({ x: x - 1, y: y - 1, })
    }

    return neighbors
  }

  /**
   * @method
   * @name getFreeTiltSides
   * @description Retorna as posições laterais a informada que estejam livres | Returns the lateral positions to the informed that are free
   * @param {{x: number, y: number}} pos Posição a conferir (em tilts) | Position to check (on tilts)
   * @param {object} neighbors Tilts dos biomas | Tilts from biomes
   * @param {object} params Parâmetros extras | Extra parameters
   *   @param {'+' | '-' | '*'} params.type Direções a serem conferidas | Directions to be given
   * @returns {{x: number, y: number}[]}
   */
  getFreeTiltSides (pos, neighbors, params = {}) {
    const { x, y, } = pos
    const { type = '+', } = params

    const positions = this.getNeighborPos({ x, y, }, { type, })

    return positions.filter((pos) => {
      const neighbor = neighbors[pos.x + 'X' + pos.y]

      return neighbor === undefined || neighbor.constructor.name === this.DividerGround.name
    })
  }

  /* -------------------------------------------------------------------------------------------------------------- */

  /**
   * @method
   * @public
   * @name setZoom
   * @description Seta o zoom da câmera | Camera zoom arrow
   * @param {number} zoom - O zoom | The zoom
   * @returns {void}
   */
  setZoom (zoom) {
    this.debug({ msg: 'Zoom definido para ' + zoom, })

    this.cameras.main.setZoom(zoom)
  }

  /**
   * @method
   * @public
   * @name zoomIn
   * @description Aumenta o zoom | Zoom in
   * @returns {void}
   */
  zoomIn () {
    const zoom = this.cameras.main.zoom + 0.01

    if (this.zoomLimitEnabled && zoom > this.zoomMax) {
      return
    }

    this.setZoom(zoom)
  }

  /**
   * @method
   * @public
   * @name zoomIn
   * @description Diminui o zoom | Zoom out
   * @returns {void}
   */
  zoomOut () {
    const zoom = this.cameras.main.zoom - 0.01

    if (this.zoomLimitEnabled && zoom < this.zoomMin) {
      return
    }

    this.setZoom(zoom)
  }

  /* -------------------------------------------------------------------------------------------------------------- */

  /**
   * @method
   * @public
   * @name setRangeRendering
   * @description Altera o range de rangerização | Change rangerization range
   * @returns {void}
   */
  setRangeRendering (range) {
    this.rangeRendering = range

    this.renderingArea(true)
  }

  /**
   * @method
   * @public
   * @name toogleZoomLimit
   * @description Ativa/Inativa o limite de zoom | Enables/Disables the zoom limit
   * @returns {void}
   */
  toogleZoomLimit () {
    this.zoomLimitEnabled = !this.zoomLimitEnabled

    this.debug({ msg: 'Limite de zoom alterado para: ' + this.zoomLimitEnabled, })
  }

  /**
   * @method
   * @public
   * @name listBiomes
   * @description Lista todos os biomas criados | List all created biomes
   * @returns {void}
   */
  listBiomes () {
    console.log(this.createdBiomes)
  }

  /**
   * @method
   * @public
   * @name goToBiome
   * @description Transporta o personagem para um determinado bioma | Transports the character to a certain biome
   * @param {string} biome Nome ou ID do bioma | Biome name or ID
   * @returns {void}
   */
  goToBiome (biomeNameOrId) {
    let center

    if (this.createdBiomes[biomeNameOrId]) {
      center = this.createdBiomes[biomeNameOrId].center
    }

    if (!center) {
      Object.values(this.createdBiomes).forEach((biome) => {
        if (biome.id === biomeNameOrId) {
          center = biome.center
        }
      })
    }

    if (!center) {
      return
    }

    this.groups.Character.list()[0].setPos(center)
  }

  /* -------------------------------------------------------------------------------------------------------------- */

  /**
   * @method
   * @public
   * @name limit
   * @description Retorna o tilt máximo que pode ser alcançado em qualquer direção | Returns the maximum tilt that can be reached in any direction
   * @returns {number}
   */
  limit () {
    return Math.floor(this.mapSize / 2)
  }

  /**
   * @method
   * @public
   * @name totalTilts
   * @description Retorna a quantidade de tilts no mundo | Returns the amount of tilts in the world
   * @returns {number}
   */
  totalTilts () {
    return this.mapSize * this.mapSize
  }

  /* -------------------------------------------------------------------------------------------------------------- */

  /**
   * @method
   * @public
   * @name debug
   * @description Exibe mensagem de debug | Display debug message
   * @param {{ msg, data, style, type}} message - Dados da mensagem | Message data
   * @param {string} message.msg - A mensagem | The message
   * @param {object} message.data - Objeto a ser exibido na mensagem | Object to be displayed in the message
   * @param {string} message.style - Estilo CSS da mensagem | Message CSS style
   * @param {string} message.type - Tipo da mensagem | Message type
   * @returns {void}
   */
  debug (message) {
    const { msg, data, style, type = 'log', } = message

    if (!this.activeDebug) { return }

    console[type]('%c' + msg, style)

    if (data) {
      console[type](data)
    }
  }

  /**
   * @method
   * @public
   * @name debugGroup
   * @description Inicia um grupo de mensagens de debug | Start a group of debug messages
   * @param {string} msg - Mensagem a ser exibida ao iniciar o grupo | Message to be displayed when starting the group
   * @returns {void}
   */
  debugGroup (msg) {
    if (!this.activeDebug) { return }

    console.group()
  }

  /**
   * @method
   * @public
   * @name debugGroupEnd
   * @description Finaliza o último grupo de mensagens de debug | End the last group of debug messages
   * @returns {void}
   */
  debugGroupEnd () {
    if (!this.activeDebug) { return }

    console.groupEnd()
  }
}
