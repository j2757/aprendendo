import { CharacterCore, } from '../core/Character'

/**
 * @class
 * @name Character
 * @description Classe responsável pelos personagens | Class responsible for the characters
 * @property {number} velocity - Velocidade do jogador | Player speed
 */
export class Character extends CharacterCore {
  constructor (game, params) {
    super(game, params)

    this.velocity = 300
  }

  /**
   * @method
   * @name create
   * @description Cria o objeto | Create the object
   * @returns {void}
   */
  create () {
    const toReturn = super.create()

    this.game.cameras.getCamera('interface').ignore(this.instance)

    return toReturn
  }
}

/**
 * @function
 * @name CharacterEntity
 * @description Carrega as mídias da entidade | Load entity media
 * @param {object} game - Instância do Phaser | Phaser instance
 */
export function CharacterEntity (game) {
  game.load.spritesheet('Character', 'assets/images/Character.png', { frameWidth: 180, frameHeight: 180, })
}
