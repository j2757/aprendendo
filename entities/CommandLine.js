import { EntitieCore, } from '../core/Entity'

/**
 * @class
 * @name CommandLine
 * @description Console | Console
 *
 * @property {object} entitie Objeto da entidade | Entity object
 * @prop {boolean} visible Se está visível | If it is visible
 */
export class CommandLine extends EntitieCore {
  constructor (game, params) {
    super(game, params)

    window.addEventListener('blur', () => {
      this.off().hide()

      this.game.controllers.powerAll('keyboard', true)
    })
  }

  /**
   * @method
   * @name create
   * @description Cria o objeto | Create the object
   * @returns {void}
   */
  create () {
    this.visible = false

    this.instance = this.game.add.dom(this.posX, this.posY).createFromCache('CommandLine')

    this.game.cameras.main.ignore(this.instance)

    this.addToGroup('Interface')

    this.fixToCamera()

    this.instance.node.addEventListener('keyup', (e) => {
      if (e.key === 'Enter') {
        const input = this.instance.node.querySelector('input')
        const val = input.value

        input.value = ''

        this.execCommand(val)
      }
    })

    return super.create()
  }

  /**
   * @method
   * @name update
   * @description Atualiza o objeto | update the object
   * @returns {void}
   */
  update () {
    return super.update()
  }

  /* -------------------------------------------------------------------------------------------------------------- */

  /**
   * @method
   * @public
   * @name controls
   * @description Define os controles | Sets controls
   * @returns {void}
   */
  controls () {
    this.game.controllers.add('keyboard', 'F12', () => this.toogle(), { delay: 100, })
  }

  /* -------------------------------------------------------------------------------------------------------------- */

  /**
   * @method
   * @public
   * @name toogle
   * @description Alterna entre visível e oculto | Switch between visible and hidden
   * @returns {void}
   */
  toogle () {
    this.visible = !this.visible

    if (this.visible) {
      this.on().show()

      this.game.controllers.powerAll('keyboard', false, 'F12')

      window.setTimeout(() => {
        this.instance.node.querySelector('input').focus()
      }, 100)
    } else {
      this.off().hide()

      this.game.controllers.powerAll('keyboard', true)
    }
  }

  /**
   * @method
   * @public
   * @name execCommand
   * @description Executa um dado comando | Execute a given command
   * @param {string | string[]} command  O comando | The command
   * @returns {void}
   */
  execCommand (command) {
    // setZoom(0.03) ; setRangeRender(125)

    if (typeof command === 'string') {
      this.execCommand(command.split(';'))
    } else {
      command.forEach((c) => {
        const cmd = c.split('(')[0].trim()
        const params = c.split('(')[1].split(')')[0].split(',')

        if (cmd === 'setZoom') {
          this.game.setZoom(Number(params[0]))
        } else if (cmd === 'setCharVelocity') {
          this.game.objects.Character.setVelocity(Number(params[0]))
        } else if (cmd === 'setRangeRender') {
          this.game.setRangeRendering(Number(params[0]))
        } else if (cmd === 'toogleZoomLimit') {
          this.game.toogleZoomLimit()
        } else if (cmd === 'listBiomes') {
          this.game.listBiomes()
        } else if (cmd === 'goToBiome') {
          this.game.goToBiome(params[0])
        }
      })
    }
  }
}

/**
 * @function
 * @name CommandLineEntity
 * @description Carrega as mídias da entidade | Load entity media
 * @param {object} game - Instância do Phaser | Phaser instance
 */
export function CommandLineEntity (game) {
  game.load.html('CommandLine', 'assets/html/CommandLine.html')
}
