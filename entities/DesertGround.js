import { GroundCore, } from '../core/Ground'

/**
 * @class
 * @name DesertGround
 * @description Terreno do deserto | Desert terrain
 */
export class DesertGround extends GroundCore {
  /**
   * @method
   * @name create
   * @description Cria o objeto | Create the object
   * @returns {void}
   */
  create () {
    this.instance = this.game.add.image(this.posX, this.posY, 'DesertGround')

    this.addToGroup('DesertGround')
    this.addToGroup('Ground')

    this.game.cameras.getCamera('interface').ignore(this.instance)

    return super.create()
  }

  /**
   * @method
   * @name update
   * @description Atualiza o objeto | update the object
   * @returns {void}
   */
  update () {
    return super.update()
  }
}

/**
 * @function
 * @name DesertGroundEntity
 * @description Carrega as mídias da entidade | Load entity media
 * @param {object} game - Instância do Phaser | Phaser instance
 */
export function DesertGroundEntity (game) {
  game.load.image('DesertGround', 'assets/images/DesertGround.png')
}
