import { GroundCore, } from '../core/Ground'

/**
 * @class
 * @name DesertGround
 * @description Terreno de terra | Earth land
 */
export class EarthGround extends GroundCore {
  /**
   * @method
   * @name create
   * @description Cria o objeto | Create the object
   * @returns {void}
   */
  create () {
    this.instance = this.game.add.image(this.posX, this.posY, 'EarthGround')

    this.addToGroup('EarthGround')
    this.addToGroup('Ground')

    this.game.cameras.getCamera('interface').ignore(this.instance)

    return super.create()
  }

  /**
   * @method
   * @name update
   * @description Atualiza o objeto | update the object
   * @returns {void}
   */
  update () {
    return super.update()
  }
}

/**
 * @function
 * @name EarthGroundEntity
 * @description Carrega as mídias da entidade | Load entity media
 * @param {object} game - Instância do Phaser | Phaser instance
 */
export function EarthGroundEntity (game) {
  game.load.image('EarthGround', 'assets/images/EarthGround.png')
}
