import { GroundCore, } from '../core/Ground'

/**
 * @class
 * @name DesertGround
 * @description Terreno do deserto | Desert terrain
 */
export class GrassGround extends GroundCore {
  /**
   * @method
   * @name create
   * @description Cria o objeto | Create the object
   * @returns {void}
   */
  create () {
    this.instance = this.game.add.image(this.posX, this.posY, 'GrassGround')

    this.addToGroup('GrassGround')
    this.addToGroup('Ground')

    this.game.cameras.getCamera('interface').ignore(this.instance)

    return super.create()
  }

  /**
   * @method
   * @name update
   * @description Atualiza o objeto | update the object
   * @returns {void}
   */
  update () {
    return super.update()
  }
}

/**
 * @function
 * @name GrassGroundEntity
 * @description Carrega as mídias da entidade | Load entity media
 * @param {object} game - Instância do Phaser | Phaser instance
 */
export function GrassGroundEntity (game) {
  game.load.image('GrassGround', 'assets/images/GrassGround.png')
}
