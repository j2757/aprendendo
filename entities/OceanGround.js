import { GroundCore, } from '../core/Ground'

/**
 * @class
 * @name OceanGround
 * @description Terreno do mar | Sea land
 */
export class OceanGround extends GroundCore {
  /**
   * @method
   * @name create
   * @description Cria o objeto | Create the object
   * @returns {void}
   */
  create () {
    this.instance = this.game.physics.add.image(this.posX, this.posY, 'OceanGround')

    this.addToGroups([
      'OceanGround',
    ])

    this.setPushable(false)

    this.game.cameras.getCamera('interface').ignore(this.instance)

    return super.create()
  }

  /**
   * @method
   * @name update
   * @description Atualiza o objeto | update the object
   * @returns {void}
   */
  update () {
    return super.update()
  }
}

/**
 * @function
 * @name OceanGroundEntity
 * @description Carrega as mídias da entidade | Load entity media
 * @param {object} game - Instância do Phaser | Phaser instance
 */
export function OceanGroundEntity (game) {
  game.load.image('OceanGround', 'assets/images/OceanGround.png')
}
