import { GroundCore, } from '../core/Ground'

/**
 * @class
 * @name RockGround
 * @description Terreno da pedreira | Quarry ground
 */
export class RockGround extends GroundCore {
  /**
   * @method
   * @name create
   * @description Cria o objeto | Create the object
   * @returns {void}
   */
  create () {
    this.instance = this.game.add.image(this.posX, this.posY, 'RockGround')

    this.addToGroup('RockGround')
    this.addToGroup('Ground')

    this.game.cameras.getCamera('interface').ignore(this.instance)

    return super.create()
  }

  /**
   * @method
   * @name update
   * @description Atualiza o objeto | update the object
   * @returns {void}
   */
  update () {
    return super.update()
  }
}

/**
 * @function
 * @name DesertGroundEntity
 * @description Carrega as mídias da entidade | Load entity media
 * @param {object} game - Instância do Phaser | Phaser instance
 */
export function RockGroundEntity (game) {
  game.load.image('RockGround', 'assets/images/RockGround.png')
}
