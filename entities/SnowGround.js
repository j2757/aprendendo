import { GroundCore, } from '../core/Ground'

/**
 * @class
 * @name SnowGround
 * @description Terreno de neve | Snow terrain
 */
export class SnowGround extends GroundCore {
  /**
   * @method
   * @name create
   * @description Cria o objeto | Create the object
   * @returns {void}
   */
  create () {
    this.instance = this.game.add.image(this.posX, this.posY, 'SnowGround')

    this.addToGroup('SnowGround')
    this.addToGroup('Ground')

    this.game.cameras.getCamera('interface').ignore(this.instance)

    return super.create()
  }

  /**
   * @method
   * @name update
   * @description Atualiza o objeto | update the object
   * @returns {void}
   */
  update () {
    return super.update()
  }
}

/**
 * @function
 * @name SnowGroundEntity
 * @description Carrega as mídias da entidade | Load entity media
 * @param {object} game - Instância do Phaser | Phaser instance
 */
export function SnowGroundEntity (game) {
  game.load.image('SnowGround', 'assets/images/SnowGround.png')
}
