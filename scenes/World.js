import Phaser from 'phaser'

import { WorldCore, } from '../core/World'

import { GrassGroundEntity, } from '../entities/GrassGround'
import { EarthGroundEntity, } from '../entities/EarthGround'
import { OceanGroundEntity, OceanGround, } from '../entities/OceanGround'
import { RockGroundEntity, } from '../entities/RockGround'
import { DesertGroundEntity, } from '../entities/DesertGround'
import { SnowGroundEntity, } from '../entities/SnowGround'
import { CharacterEntity, Character, } from '../entities/Character'

import { EarthBiome, } from '../biomes/Earth'
import { RockBiome, } from '../biomes/Rock'
import { GrassBiome, } from '../biomes/Grass'
import { DesertBiome, } from '../biomes/Desert'
import { SnowBiome, } from '../biomes/Snow'

/**
 * @class
 * @name World
 * @description Classe responsável pela geração de mundos | Class responsible for generating worlds
 * @property {boolean} activeDebug - Se deve exibir mensagens de debug | Se deve exibir mensagens de debug
 * @property {number} mapSize - Tamanho do mundo em tilts | World size in tilts
 */
export default class World extends WorldCore {
  constructor () {
    super({ key: 'World', })

    this.activeDebug = true

    this.mapSize = 250 // 100
    this.DividerGround = OceanGround
  }

  /* -------------------------------------------------------------------------------------------------------------- */

  /**
   * @method
   * @public
   * @name preload
   * @description Carrega mídias | Load media
   * @returns {void}
   */
  preload () {
    this.addEntities([
      CharacterEntity,
      GrassGroundEntity,
      EarthGroundEntity,
      RockGroundEntity,
      OceanGroundEntity,
      DesertGroundEntity,
      SnowGroundEntity,
    ])

    this.createGroups([
      'EarthGround',
      'OceanGround',
      'RockGround',
      'GrassGround',
      'DesertGround',
      'SnowGround',
      'OceanGround',
    ])

    super.preload()
  }

  /**
   * @method
   * @public
   * @name create
   * @description Cria objetos | Create objects
   * @returns {void}
   */
  create () {
    this.addMainBiome(GrassBiome)

    this.addBiomes([
      {
        Biome: RockBiome,
        count: Phaser.Math.Between(1, 2),
      },
      {
        Biome: EarthBiome,
        count: 1,
      },
      {
        Biome: DesertBiome,
        count: 2,
        separable: false,
      },
      {
        Biome: SnowBiome,
        count: Phaser.Math.Between(1, 2),
        separable: false,
      },
      {
        Biome: GrassBiome,
        count: 8,
      },
    ])

    this.addObjects([
      (new Character(this, { id: 'Character', }))
        .setPos({ x: 0, y: 0, }),
    ])

    super.create()
  }

  /**
   * @method
   * @public
   * @name update
   * @description Atualiza objetos | Update objects
   * @returns {void}
   */
  update () {
    this.controls()

    super.update()
  }
}
